from distutils.core import setup

setup(
    name='bestutils',
    version='0.1dev',
    packages=['bestutils',],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
)
