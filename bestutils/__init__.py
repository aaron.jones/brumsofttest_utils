from __future__ import print_function
import sys
import os
#import subprocess
import requests

def do_upload(path,location='test',example=False):
    ''' file is the file to be uploaded.
    test may be `test`, `session` or `commit`
    and specifies where the file should go'''

    if example:
        os.environ['BESTDATASERVER']='https://example.com'
        os.environ['BEST_TEST_NUMBER'] = '1'
        os.environ['BEST_SESSION_NUMBER'] = '10'

    baseurl = os.environ['BESTDATASERVER']
    baseurl += "/filestorage/upload/"
    if location == 'test':
        url = baseurl+'test/'+os.environ['BEST_TEST_NUMBER']
    elif location == 'session':
        url = baseurl+'session/'+os.environ['BEST_SESSION_NUMBER']
    elif location == 'commit':
        sha1 = os.popen('git rev-parse HEAD').read().strip()
        url = baseurl + 'commit/' + sha1
    else:
        raise NotImplementedError(str(location)+' is not implemented')

    print('Uploading: '+str(path)+" To: "+url)
    with open(path, 'rb') as f:
        r = requests.post(url, files={'file': f},verify=False)
    print("Server Returns: " + str(r.text))
    r.raise_for_status()

